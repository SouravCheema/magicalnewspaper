//
//  ViewController.swift
//  WildCardMagicApp
//
//  Created by Sourav Dewett on 2019-04-07.
//  Copyright © 2019 Sourav Dewett. All rights reserved.
//

import UIKit
import SceneKit
import ARKit
import ReplayKit
import TwitterKit
import IOSurface



class ViewController: UIViewController, ARSCNViewDelegate,ARSessionDelegate, RPPreviewViewControllerDelegate, UIImagePickerControllerDelegate {

    @IBOutlet weak var warningButton: UILabel!
    @IBOutlet var camvie: UIView!
    @IBOutlet var sceneView: ARSCNView!
    var videoNode = SKVideoNode(fileNamed: "")
    let targetAnchor : ARImageAnchor? = nil
    let recorder = RPScreenRecorder.shared()
    var frontCamera  = AVCaptureDevice.default(.builtInWideAngleCamera, for: .video, position: .front)
    var rearCamera = AVCaptureDevice.default(.builtInWideAngleCamera, for: .video, position: .back)
    var captureSession : AVCaptureSession?
    var captureDevice: AVCaptureDevice?
    var videoCaptureLayer : AVCaptureVideoPreviewLayer?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set the view's delegate
        sceneView.delegate = self
        
        // Show statistics such as fps and timing information
        sceneView.showsStatistics = false
        
        if #available(iOS 12.0, *) {
            let captureDevice = AVCaptureDevice.default(.builtInWideAngleCamera, for: .video, position: .back)
            do {
                let input = try AVCaptureDeviceInput(device: captureDevice!)
                captureSession = AVCaptureSession()
                captureSession?.addInput(input)
                
                self.videoCaptureLayer = AVCaptureVideoPreviewLayer(session: captureSession!)
                self.videoCaptureLayer?.frame = view.layer.bounds
//                camvie.layer.addSublayer(videoCaptureLayer!)
                captureSession?.startRunning()
            }
            catch {
                print(error)
            }
        }
      
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Create a session configuration
        let configuration = ARImageTrackingConfiguration()
        
        if  let trackedImages = ARReferenceImage.referenceImages(inGroupNamed: "NewsPaperImages", bundle: Bundle.main) {
            
            configuration.trackingImages = trackedImages
            configuration.maximumNumberOfTrackedImages = 1
            
            warningButton.numberOfLines = 2
            
        }

        // Run the view's session
        sceneView.session.run(configuration)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Pause the view's session
        sceneView.session.pause()
        
    }
    
    
        

    // MARK: - ARSCNViewDelegate
    
    func renderer(_ renderer: SCNSceneRenderer, nodeFor anchor: ARAnchor) -> SCNNode? {
        let node = SCNNode()

        if let imageAnchor = anchor as? ARImageAnchor {
            if(anchor.name == "ImageS") {
            let videoNode = SKVideoNode(fileNamed: "riverBeauty.mp4")
            videoNode.play()
            
                print("Yes it is an image")
                
                
                let videoScene = SKScene(size: CGSize(width: 480, height: 360))
                
                videoNode.position = CGPoint(x: videoScene.size.width/2, y: videoScene.size.height/2)
                videoNode.yScale = -1.0
                
                videoScene.addChild(videoNode)
                
                
                
                let plane = SCNPlane(width: imageAnchor.referenceImage.physicalSize.width, height: imageAnchor.referenceImage.physicalSize.height)
                
                
                plane.firstMaterial?.diffuse.contents = videoScene
                
                let planeNode = SCNNode(geometry: plane)
                planeNode.eulerAngles.x = -.pi/2
                
                node.addChildNode(planeNode)
            
        
            }
            
            if(anchor.name == "Image2_River") {
                
                let videoNode = SKVideoNode(fileNamed: "Floods.mp4")
                videoNode.play()
                
                
                print("Yes it is an image")
                
                
                let videoScene = SKScene(size: CGSize(width: 480, height: 360))
                
                videoNode.position = CGPoint(x: videoScene.size.width/2, y: videoScene.size.height/2)
                videoNode.yScale = -1.0
                
                videoScene.addChild(videoNode)
                
                
                
                let plane = SCNPlane(width: imageAnchor.referenceImage.physicalSize.width, height: imageAnchor.referenceImage.physicalSize.height)
                
                
                plane.firstMaterial?.diffuse.contents = videoScene
                
                let planeNode = SCNNode(geometry: plane)
                planeNode.eulerAngles.x = -.pi/2
                
                node.addChildNode(planeNode)
            }
            
            if(anchor.name == "Image3_River") {
                
                let videoNode = SKVideoNode(fileNamed: "testMatch.mp4")
                videoNode.play()
                
                
                print("Yes it is an image")
                
                
                let videoScene = SKScene(size: CGSize(width: 480, height: 360))
                
                videoNode.position = CGPoint(x: videoScene.size.width/2, y: videoScene.size.height/2)
                videoNode.yScale = -1.0
                
                videoScene.addChild(videoNode)
                
                
                
                let plane = SCNPlane(width: imageAnchor.referenceImage.physicalSize.width, height: imageAnchor.referenceImage.physicalSize.height)
                
                
                plane.firstMaterial?.diffuse.contents = videoScene
                
                let planeNode = SCNNode(geometry: plane)
                planeNode.eulerAngles.x = -.pi/2
                
                node.addChildNode(planeNode)
            }
            
            if(anchor.name == "Image_Accident") {
                
                let videoNode = SKVideoNode(fileNamed: "carAccident.mp4")
                videoNode.play()
                
                
                print("Yes it is an image")
                
                
                let videoScene = SKScene(size: CGSize(width: 480, height: 360))
                
                videoNode.position = CGPoint(x: videoScene.size.width/2, y: videoScene.size.height/2)
                videoNode.yScale = -1.0
                
                videoScene.addChild(videoNode)
                
                
                
                let plane = SCNPlane(width: imageAnchor.referenceImage.physicalSize.width, height: imageAnchor.referenceImage.physicalSize.height)
                
                
                plane.firstMaterial?.diffuse.contents = videoScene
                
                let planeNode = SCNNode(geometry: plane)
                planeNode.eulerAngles.x = -.pi/2
                
                node.addChildNode(planeNode)
            }
            
            if(anchor.name == "Cricket") {
                
                let videoNode = SKVideoNode(fileNamed: "testMatch.mp4")
                videoNode.play()
                
                
                print("Yes it is an image")
                
                
                let videoScene = SKScene(size: CGSize(width: 480, height: 360))
                
                videoNode.position = CGPoint(x: videoScene.size.width/2, y: videoScene.size.height/2)
                videoNode.yScale = -1.0
                
                videoScene.addChild(videoNode)
                
                
                
                let plane = SCNPlane(width: imageAnchor.referenceImage.physicalSize.width, height: imageAnchor.referenceImage.physicalSize.height)
                
                
                plane.firstMaterial?.diffuse.contents = videoScene
                
                let planeNode = SCNNode(geometry: plane)
                planeNode.eulerAngles.x = -.pi/2
                
                node.addChildNode(planeNode)
            }
            if(anchor.name == "Image_Crane") {
                
                let videoNode = SKVideoNode(fileNamed: "Crane.mp4")
                videoNode.play()
                
                
                print("Yes it is an image")
                
                
                let videoScene = SKScene(size: CGSize(width: 480, height: 360))
                
                videoNode.position = CGPoint(x: videoScene.size.width/2, y: videoScene.size.height/2)
                videoNode.yScale = -1.0
                
                videoScene.addChild(videoNode)
                
                
                
                let plane = SCNPlane(width: imageAnchor.referenceImage.physicalSize.width, height: imageAnchor.referenceImage.physicalSize.height)
                
                
                plane.firstMaterial?.diffuse.contents = videoScene
                
                let planeNode = SCNNode(geometry: plane)
                planeNode.eulerAngles.x = -.pi/2
                
                node.addChildNode(planeNode)
            }
            if(anchor.name == "Swimming") {
                
                let videoNode = SKVideoNode(fileNamed: "FlipDiving.mp4")
                videoNode.play()
                
                
                print("Yes it is an image")
                
                
                let videoScene = SKScene(size: CGSize(width: 480, height: 360))
                
                videoNode.position = CGPoint(x: videoScene.size.width/2, y: videoScene.size.height/2)
                videoNode.yScale = -1.0
                
                videoScene.addChild(videoNode)
                
                
                
                let plane = SCNPlane(width: imageAnchor.referenceImage.physicalSize.width, height: imageAnchor.referenceImage.physicalSize.height)
                
                
                plane.firstMaterial?.diffuse.contents = videoScene
                
                let planeNode = SCNNode(geometry: plane)
                planeNode.eulerAngles.x = -.pi/2
                
                node.addChildNode(planeNode)
            }
            if(anchor.name == "Tennis") {
                
                let videoNode = SKVideoNode(fileNamed: "Tennis.mp4")
                videoNode.play()
                
                
                print("Yes it is an image")
                
                
                let videoScene = SKScene(size: CGSize(width: 480, height: 360))
                
                videoNode.position = CGPoint(x: videoScene.size.width/2, y: videoScene.size.height/2)
                videoNode.yScale = -1.0
                
                videoScene.addChild(videoNode)
                
                
                
                let plane = SCNPlane(width: imageAnchor.referenceImage.physicalSize.width, height: imageAnchor.referenceImage.physicalSize.height)
                
                
                plane.firstMaterial?.diffuse.contents = videoScene
                
                let planeNode = SCNNode(geometry: plane)
                planeNode.eulerAngles.x = -.pi/2
                
                node.addChildNode(planeNode)
            }
        }
        return node
        

    }
    
    func renderer(_ renderer: SCNSceneRenderer, didRemove node: SCNNode, for anchor: ARAnchor) {
        guard let _ = anchor as? ARImageAnchor else {
            return
        }
        node.enumerateChildNodes { (node, _) in
            node.removeFromParentNode()
        }
    }
    
    
    @IBAction func onRecordTapped(_ sender: Any) {
        self.recorder.startRecording { (error) in
            if let error = error {
                print(error)
            }
        }
    }
    
    
    @IBAction func onStopTapped(_ sender: Any) {
        self.recorder.stopRecording { (previewVC, error) in
            if let previewVC = previewVC {
                previewVC.previewControllerDelegate = self
                
//                let button = UIButton()
//                button.frame = CGRect(x: 50, y: 100, width: 90, height: 30)
//                button.setTitle("Share", for: .normal)
////                let stackView = UIStackView()
////                stackView.addArrangedSubview(button)
//
//                button.addTarget(self, action: #selector(self.shareButtonTapped), for: .touchUpInside)
//
//                previewVC.view.addSubview(button)
                self.present(previewVC, animated: true, completion: nil)
            }
            
            if let error = error {
                print(error)
            }
        }
    }
    
    func previewControllerDidFinish(_ previewController: RPPreviewViewController) {
        dismiss(animated: true, completion: nil)
    }
    
 

    
    
    @IBAction func ShareButtonTapped(_ sender: Any) {
        
        
        TWTRTwitter.sharedInstance().start(withConsumerKey: "TJrfqvHcDLrio3ISlOi5aVr8k", consumerSecret: "xEJJprYMRj42OtvirMkK98wVKQbXJDPhoaWBy7j2sa7O6P1v7c")
        
        
        if (TWTRTwitter.sharedInstance().sessionStore.hasLoggedInUsers()) {
            // App must have at least one logged-in user to compose a Tweet

            guard let shareImg2 = UIImage.init(named: "twitter.png") else{
                print("failed init share img")
                return
            }
            //let shareImg = UIImage.init(named: "mountain")!
            let composer = TWTRComposerViewController.init(initialText: "UK flag picture will be tweeted", image: shareImg2, videoURL: nil)
            composer.delegate = self as! TWTRComposerViewControllerDelegate
            composer.show(self.navigationController!, sender: (Any).self)

        } else {
            // Log in, and then check again
            TWTRTwitter.sharedInstance().logIn { session, error in
                if session != nil { // Log in succeeded

                    guard let shareImg2 = UIImage.init(named: "twitter.png") else{
                        print("failed init share img")
                        return
                    }
                    //let shareImg = UIImage.init(named: "mountain")!
                    let composer = TWTRComposerViewController.init(initialText: "USA flag picture will be tweeted", image: shareImg2, videoURL: nil)
                    composer.delegate = self as! TWTRComposerViewControllerDelegate
                    self.present(composer, animated: true, completion: nil)



                } else {
                    let alert = UIAlertController(title: "No Twitter Accounts Available", message: "You must log in before presenting a composer.", preferredStyle: .alert)
                    self.present(alert, animated: false, completion: nil)
                }
            }

        }
        
        
        
        
    }
    
    func switchToFrontCamera()  {
        if frontCamera?.isConnected == true{
            captureSession?.stopRunning()
            let captureDevice  = AVCaptureDevice.default(.builtInWideAngleCamera, for: .video, position: .front)
            do {
                let input = try AVCaptureDeviceInput(device: captureDevice!)
                captureSession = AVCaptureSession()
                captureSession?.addInput(input)
                
                self.videoCaptureLayer = AVCaptureVideoPreviewLayer(session: captureSession!)
                self.videoCaptureLayer?.frame = view.layer.bounds
//                camvie.layer.addSublayer(videoCaptureLayer!)
                captureSession?.startRunning()
            }
            catch {
                print(error)
            }
        }
    }
    
    
    func switchToBackCamera() {
        if rearCamera?.isConnected == true{
            captureSession?.stopRunning()
            let captureDevice  = AVCaptureDevice.default(.builtInWideAngleCamera, for: .video, position: .back)
            do {
                let input = try AVCaptureDeviceInput(device: captureDevice!)
                captureSession = AVCaptureSession()
                captureSession?.addInput(input)
                
                self.videoCaptureLayer = AVCaptureVideoPreviewLayer(session: captureSession!)
                self.videoCaptureLayer?.frame = view.layer.bounds
//                camvie.layer.addSublayer(videoCaptureLayer!)
                captureSession?.startRunning()
            }
            catch {
                print(error)
            }
        }
    }
    
    
    
    @IBAction func SwitchCamera(_ sender: Any) {
        
        guard  let currentCameraInput : AVCaptureInput = captureSession?.inputs.first else {
            return
        }
        if let input = currentCameraInput as? AVCaptureDeviceInput {
            if input.device.position == .back {
                switchToFrontCamera()
            }
            
            if input.device.position == .front {
                switchToBackCamera()
            }
        }
    }

}
