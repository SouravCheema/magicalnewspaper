//
//  startScreen.swift
//  WildCardMagicApp
//
//  Created by Sourav Dewett on 2019-04-22.
//  Copyright © 2019 Sourav Dewett. All rights reserved.
//

import UIKit

class startScreen: UIViewController {

    @IBOutlet var startScreenView: UIView!
    @IBOutlet weak var textLabel: UILabel!
    
    let image : UIImage = UIImage(named: "NewsPaperImg.jpg")!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let gradient = CAGradientLayer()
        
        gradient.frame = startScreenView.bounds
        gradient.colors = [UIColor.white.cgColor, UIColor.orange.cgColor]
        startScreenView.layer.insertSublayer(gradient, at: 0)
        
        self.textLabel.numberOfLines = 7
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
